import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {

  // Go to https://w3.cathaylife.com.tw/PTWeb/servlet/HttpDispatcher/PTEX_0001/index
  await page.goto('https://w3.cathaylife.com.tw/PTWeb/servlet/HttpDispatcher/PTEX_0001/index');

  // Go to https://w3.cathaylife.com.tw/eai/ZPWeb/login.jsp
  await page.goto('https://w3.cathaylife.com.tw/eai/ZPWeb/login.jsp');

  // Click input[name="username"]
  await page.locator('input[name="username"]').click();

  // Fill input[name="username"]
  await page.locator('input[name="username"]').fill('00891465');

  // Click input[type="password"]
  await page.locator('input[type="password"]').click();

  // Fill input[type="password"]
  await page.locator('input[type="password"]').fill('sa124124@');

  // Press Enter
  await page.locator('input[type="password"]').press('Enter');
  await expect(page).toHaveURL('https://w3.cathaylife.com.tw/eai/ZPWeb/login.jsp');

  // Click input[name="username"]
  await page.locator('input[name="username"]').click();

  // Fill input[name="username"]
  await page.locator('input[name="username"]').fill('00891465');

  // Click input[type="password"]
  await page.locator('input[type="password"]').click();

  // Fill input[type="password"]
  await page.locator('input[type="password"]').fill('sa124124@');

  // Click a:has-text("登入")
  await page.locator('a:has-text("登入")').click();
  await expect(page).toHaveURL('https://w3.cathaylife.com.tw/eai/ZPWeb/login.jsp');

  // Click input[name="username"]
  await page.locator('input[name="username"]').click();

  // Fill input[name="username"]
  await page.locator('input[name="username"]').fill('00891465');

  // Click input[type="password"]
  await page.locator('input[type="password"]').click();

  // Fill input[type="password"]
  await page.locator('input[type="password"]').fill('Sa124124@');

  // Click a:has-text("登入")
  await page.locator('a:has-text("登入")').click();
  await expect(page).toHaveURL('https://w3.cathaylife.com.tw/PTWeb/servlet/HttpDispatcher/PTEX_0001/index');

  // Click text=系統專區
  await page.locator('text=系統專區').click();

  // Click text=壽險核心系統 >> nth=1
  const [page1] = await Promise.all([
    page.waitForEvent('popup'),
    page.locator('text=壽險核心系統').nth(1).click()
  ]);

  // Click text=理賠系統
  await page1.frameLocator('frame[name="leftFrame"]').locator('text=理賠系統').click();
  await expect(page1).toHaveURL('https://was2.cathaylife.com.tw/AAWeb/servlet/HttpDispatcher/SysLogin/prompt');

  // Click text=查詢作業
  await page1.frameLocator('frame[name="leftFrame"]').locator('text=查詢作業').click();

  // Click text=5.案件資料查詢
  await page1.frameLocator('frame[name="leftFrame"]').locator('text=5.案件資料查詢').click();

  // Double click input[name="APLY_NO"]
  await page1.frameLocator('frame[name="leftFrame"]').frameLocator('iframe[name="mainFrame"]').locator('input[name="APLY_NO"]').dblclick();

  // Click input[name="APLY_NO"]
  await page1.frameLocator('frame[name="leftFrame"]').frameLocator('iframe[name="mainFrame"]').locator('input[name="APLY_NO"]').click();

  // Fill input[name="APLY_NO"]
  await page1.frameLocator('frame[name="leftFrame"]').frameLocator('iframe[name="mainFrame"]').locator('input[name="APLY_NO"]').fill('2210141960001');

  // Click input:has-text("查詢")
  page1.once('dialog', dialog => {
    console.log(`Dialog message: ${dialog.message()}`);
    dialog.dismiss().catch(() => {});
  });
  await page1.frameLocator('frame[name="leftFrame"]').frameLocator('iframe[name="mainFrame"]').locator('input:has-text("查詢")').click();

  // Click input[name="APLY_NO"]
  await page1.frameLocator('frame[name="leftFrame"]').frameLocator('iframe[name="mainFrame"]').locator('input[name="APLY_NO"]').click();

  // Click input[name="APLY_NO"]
  await page1.frameLocator('frame[name="leftFrame"]').frameLocator('iframe[name="mainFrame"]').locator('input[name="APLY_NO"]').click();

  // Click input[name="APLY_NO"]
  await page1.frameLocator('frame[name="leftFrame"]').frameLocator('iframe[name="mainFrame"]').locator('input[name="APLY_NO"]').click();

  // Fill input[name="APLY_NO"]
  await page1.frameLocator('frame[name="leftFrame"]').frameLocator('iframe[name="mainFrame"]').locator('input[name="APLY_NO"]').fill('22101401960001');

  // Click input:has-text("查詢")
  const [page2] = await Promise.all([
    page1.waitForEvent('popup'),
    page1.frameLocator('frame[name="leftFrame"]').frameLocator('iframe[name="mainFrame"]').locator('input:has-text("查詢")').click()
  ]);

});